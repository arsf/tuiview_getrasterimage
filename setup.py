#!/usr/bin/env python
"""
This file has been created by NERC-ARF Data Analysis Node and
is licensed under the GPL v3 Licence. A copy of this
licence is available to download with this file.
"""
from distutils.core import setup, Extension
import os
import sys
import numpy
from osgeo import gdal

def getGDALFlags():
    """
    Return the flags needed to link in GDAL as a dictionary

    From TuiView
    """
    extraargs = {}
    # don't use the deprecated numpy api
    extraargs['define_macros'] = [('NPY_NO_DEPRECATED_API', 'NPY_1_7_API_VERSION')]

    if sys.platform == 'win32':
        # Windows - rely on %GDAL_HOME% being set and set 
        # paths appropriately
        gdalhome = os.getenv('GDAL_HOME')
        if gdalhome is None:
            raise SystemExit("need to define %GDAL_HOME%")
        extraargs['include_dirs'] = [os.path.join(gdalhome, 'include')]
        extraargs['library_dirs'] = [os.path.join(gdalhome, 'lib')]
        extraargs['libraries'] = ['gdal_i']
    else:
        # Unix - can do better with actual flags using gdal-config
        import subprocess
        try:
            cflags = subprocess.check_output(['gdal-config', '--cflags'])
            if sys.version_info[0] >= 3:
                cflags = cflags.decode()
            extraargs['extra_compile_args'] = cflags.strip().split()

            ldflags = subprocess.check_output(['gdal-config', '--libs'])
            if sys.version_info[0] >= 3:
                ldflags = ldflags.decode()
            extraargs['extra_link_args'] = ldflags.strip().split()
        except OSError:
            raise SystemExit("can't find gdal-config - GDAL development files need to be installed")
    return extraargs

rasterextkwargs = {'name':'getRasterImage',
                   'sources':['src/getRasterImage.cpp']}
# add gdalargs
rasterextkwargs.update(getGDALFlags())
rasterextkwargs['extra_compile_args'].extend(['-lsrc/getRasterImage.h', '--std=c++14'])


#C++ setup gcc
setup(name='getRasterImage',version='1.0',
      description='C++ Extension for TuiView',
      author='Gareth Jones (NERC-ARF-DAN)',
      author_email='nerc-arf-processing@pml.ac.uk',
      ext_modules = [Extension(**rasterextkwargs)])
