/*
 * Source file for C++ Module for Tuiview
 * initializes and calls template class in getRasterImage.h
 *
 * Author: Gareth Jones
 *
 * Known Issues: N/A
 *
 * Available functions: 
 * getRasterImage_getImage(): creates Template Class rasterReader Object and
 *                            runs runGetImage()
 *
 * This file has been created by NERC-ARF Data Analysis Node and
 * is licensed under the GPL v3 Licence. A copy of this
 * licence is available to download with this file.
 *
 */
#include "getRasterImage.h"


using namespace std;
GDALDataset *getGdalDataset(PyObject *self);
const char *getGDALDataType(GDALDataset *gdalDataset);
// initialise functions that will be used.

// Create python methods used by python to initialise the class.
// needs to be extern "C" as python can only initialise from C calls not C++.
extern "C" {
static PyObject *getRasterImage_getImage(PyObject *self, PyObject *args);
static PyMethodDef getRasterImageMethods[] = {
    {"getImage", (PyCFunction)getRasterImage_getImage, METH_VARARGS,
     "main get image method for Rasters"},
    {NULL, NULL, 0, NULL}};

PyMODINIT_FUNC initgetRasterImage(void) {
    (void)Py_InitModule("getRasterImage", getRasterImageMethods);
    import_array();
}
}

static PyObject *getRasterImage_getImage(PyObject *self, PyObject *args) {
    // initial function which is called from python,
    // needs to be static otherwise python can't call it.
    PyObject *selectedovi;
    PyObject *viewerLUT;
    // process the arguments passed from python and place them into the
    // variables created earlier.
    if (!PyArg_ParseTuple(args, "OOO", &self, &selectedovi, &viewerLUT)) {
        ERROR("couldn't parse python arguments");
        return NULL;
    }
    GDALDataset *gdalDataset = getGdalDataset(self);
    const char *gdalDataType = getGDALDataType(gdalDataset);
    if (strcmp(gdalDataType, "Byte") == 0) {
        rasterReader<uint8_t> getImageClass(self, selectedovi, viewerLUT,
                                            gdalDataset, gdalDataType);
        return getImageClass.runGetImage();
    } else if (strcmp(gdalDataType, "UInt16") == 0) {
        rasterReader<uint16_t> getImageClass(self, selectedovi, viewerLUT,
                                             gdalDataset, gdalDataType);
        return getImageClass.runGetImage();
    } else if (strcmp(gdalDataType, "Int16") == 0) {
        rasterReader<int16_t> getImageClass(self, selectedovi, viewerLUT,
                                            gdalDataset, gdalDataType);
        return getImageClass.runGetImage();
    } else if (strcmp(gdalDataType, "UInt32") == 0) {
        rasterReader<uint32_t> getImageClass(self, selectedovi, viewerLUT,
                                             gdalDataset, gdalDataType);
        return getImageClass.runGetImage();
    } else if (strcmp(gdalDataType, "Int32") == 0) {
        rasterReader<int32_t> getImageClass(self, selectedovi, viewerLUT,
                                            gdalDataset, gdalDataType);
        return getImageClass.runGetImage();
    } else if (strcmp(gdalDataType, "Float32") == 0) {
        rasterReader<float> getImageClass(self, selectedovi, viewerLUT,
                                          gdalDataset, gdalDataType);
        return getImageClass.runGetImage();
    } else if (strcmp(gdalDataType, "Float64") == 0) {
        rasterReader<double> getImageClass(self, selectedovi, viewerLUT,
                                           gdalDataset, gdalDataType);
        return getImageClass.runGetImage();
    }
    ERROR("couldn't find datatype, please file a bug report with the file type "
          "so we can add the type in.");
    return Py_BuildValue("OOO", Py_None, Py_None, Py_None);
}

GDALDataset *getGdalDataset(PyObject *self) {
    PyObject *gdalDataset = PyObject_GetAttrString(self, "gdalDataset");
    PyObject *fileString =
        (PyObject *)PyObject_CallMethod(gdalDataset, "GetFileList", (""), NULL);
    GDALDataset *gdalDatasetC = (GDALDataset *)GDALOpen(
        PyString_AsString(PyList_GetItem((PyObject *)fileString, 0)),
        GA_ReadOnly);
    return gdalDatasetC;
}

const char *getGDALDataType(GDALDataset *gdalDataset) {
    const char *dataType =
        GDALGetDataTypeName(gdalDataset->GetRasterBand(1)->GetRasterDataType());
    return dataType;
}
