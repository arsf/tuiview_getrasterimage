/*
 * C++ Module for Tuiview to increase performance
 *
 * Author: Gareth Jones (gej)
 *
 *
 * License restrictions: Uses GDAL subject to X/11 MIT license restrictions
 *                       Uses NumPy subject to New-BSD license restrictions
 *
 * Available functions: N/A all must be called from class
 * This file has been created by NERC-ARF Data Analysis Node and
 * is licensed under the GPL v3 Licence. A copy of this
 * licence is available to download with this file.
 *
 */

#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "gdal_priv.h"
#include "cpl_conv.h"
#include <numpy/arrayobject.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
#include <future>
#include <thread>
#include <chrono>
#include <mutex>
#include <ctime>
#include <sys/wait.h>
#include <typeinfo>
#include <string.h>
#include <iomanip>

// define text colours and ERROR/WARNING functions.
// if DEBUGTEXT is not defined then output functions other than ERROR and WARN
// do nothing, else they will print out.
//#define DEBUGTEXT
#define RED "\033[31m"
#define REDBOLD "\033[1m\033[31m"
#define RESET "\033[0m"
#define GREEN "\033[32m"
#define BLUE "\033[34m"
#define PURPLE "\033[35m"
#define PURPLEBOLD "\033[1m\033[35m"
#define ERROR(x) std::cerr << REDBOLD << x << RESET << "\n"

#ifndef DEBUGTEXT
#define OUT(x)
#define VAR(x)
#define ENTER(x)
#define EXIT(x)
#define FORLOOPSTART(x)
#define FORLOOPEND(x)
#define WARN(x)
#else
#define OUT(x) std::cout << GREEN << x << RESET << "\n"
#define VAR(x) std::cout << BLUE << x << RESET << "\n"
#define ENTER(x) std::cout << PURPLE << "ENTERED " << x << RESET << "\n"
#define EXIT(x) std::cout << PURPLEBOLD << "EXITING " << x << RESET << "\n"
#define WARN(x) std::cerr << RED << x << RESET << "\n"
#endif
#define START std::clock()
#define TIME(x, y)                                                             \
    std::cout << REDBOLD << x                                                  \
              << " time: " << ((std::clock() - y) / (double)CLOCKS_PER_SEC)    \
              << RESET << "\n";

// Define the rasterReader class as a template class
// this allows us to compile the class to use multiple types without duplicating
// code.
template <class T> class rasterReader {
    /*
     * class rasterReader
     * template class, must be initialized with the type required
     *
     * Available functions:
     * rasterReader(): Constructor
     * runGetImage(): sets up and runs the reader
     */

  private:
    // define private variables. these don't need to be and thus can't be
    // accessed outside of the class. This means they can't be edited in
    // a different thread which would cause problems.
    uint8_t MASK_BACKGROUND_VALUE, MASK_IMAGE_VALUE, MASK_NODATA_VALUE;
    double fullrespixperovpix, selectedOviFullResPix, imgPixPerWinPixCoord;
    double pixTopC, pixBottomC, pixLeftC, pixRightC;
    double dspRastRightC, dspRastLeftC, dspRastTopC, dspRastBottomC;
    double dspLeftExtra, dspRightExtra, dspTopExtra, dspBottomExtra;
    long ovtop, ovleft, ovbottom, ovright, ovxsize, ovysize;
    long dspRastRight, dspRastLeft, dspRastTop, dspRastBottom;
    long dspRastXSize, dspRastYSize;
    long dspRastAbsLeft, dspRastAbsTop, dspRastAbsRight, dspRastAbsBottom;
    long RasterYSizeC, RasterXSizeC, indexC, dspHeightC, dspWidthC;
    long pixTop, pixBottom, pixLeft, pixRight;
    PyObject *selectedovi;
    PyListObject *noDataValues = NULL;
    PyObject *stretch;
    PyObject *viewerLUT;
    PyObject *self;
    PyObject *bands;
    PyObject *coordmgr;
    GDALDataset *gdalDataset[3];
    long dataslice[2][2];
    char *gdalDataTypeName;

    // define the global vectors to be used.
    std::vector<uint8_t> *mask;
    std::vector<int> nodata_mask;
    std::vector<T> data;
    std::vector<std::vector<T>> *datalist;
    // define the mutex locks which are used for thread safety when
    // some threads may access the same global variables.
    // needed otherwise there will be segfaults
    std::mutex rasterIO_mutex;

  public:
    // define the functions as public, this means they can be accessed from
    // anywhere
    // may change these to private as they don't need to be
    // and probably shouldn't be accessed outside of the class.(thoughts?)
    static void rasterReaderInit(PyObject *self, PyObject *args);
    rasterReader();
    rasterReader(PyObject *self, PyObject *selectedovi, PyObject *viewerLUT,
                 GDALDataset *gdalDataset, const char *gdalDataType);
    PyObject *runGetImage();
    int setInitialVariables();
    int checkCoords();
    int calculateValues();
    void checkImgPixSize();
    void fillMask();
    PyObject *rgbLut(bool tuple);
    PyObject *singleLut(bool tuple);
    int runLoop(int currentBand, int bandSize, bool tuple);
    void noDataSetup(int bandNum, std::vector<T> localData);
    PyObject *build_PyReturn(bool rgb, int bandSize);
    void noDataMaskFill();
    std::vector<T> fillData_zoomed_out(GDALRasterBand *band);
    std::vector<T> fillData_zoomed_in(GDALRasterBand *band);
    std::vector<T> replicateArray(T *dataTmp);
    void meshgrid(double startx, double endx, double starty, double endy,
                  std::vector<double> *cols, std::vector<double> *rows);
    void multiply(std::vector<double> *arr, double multiply, long startx,
                  long endx, long starty, long endy);
    std::vector<T> resizeArray(std::vector<T> arr, long arrWidth,
                               long arrHeight, long imgWidth, long imgHegith);
    std::vector<int> compareArrToInt(std::vector<T> arr, long condit);
    std::vector<int> compareArr8bitToInt(std::vector<uint8_t> *arr, int condit);
    std::vector<int> logical_and_arrays(std::vector<int> arr1,
                                        std::vector<int> arr2);
    void clearVariables();
    enum NPY_TYPES gdalToNPYDatatype(const char *gdalDatatype);
    // arrPos is an inline function as it's used often, and so making it inline
    // means less need to put functions on the stack.
    inline long arrPos(long x, long y, long height) {
        return (x * height) + y;
    }

};

// implementation of the default rasterReader constructor
// we don't want this to ever be used as we need some things
// from python to work, but it should be defined anyway.
template <class T> rasterReader<T>::rasterReader() {
}

/*// rasterReaderInit function, this is needed so python knows about this class
// although, this may not be needed.. will test.
template <class T>
void rasterReader<T>::rasterReaderInit(PyObject *self, PyObject *args) {
}*/

// actual implementation of the rasterReader constructor that will be used
// this puts the python data into the class variables so we can use them later.
template <class T>
rasterReader<T>::rasterReader(PyObject *self, PyObject *selectedovi,
                              PyObject *viewerLUT, GDALDataset *gdalDataset,
                              const char *gdalDataType) {
    ENTER("Constructor");
    this->selectedovi = selectedovi;
    this->viewerLUT = viewerLUT;
    this->self = self;
    // we only need one of the gdalDataset array to be filled.
    // the rest need to be opened later to get a new handle to the dataset
    // this is for thread safety.
    this->gdalDataset[0] = gdalDataset;
    this->gdalDataTypeName = const_cast<char *>(gdalDataType);
    EXIT("Constructor");
}

#include <cxxabi.h>
// implementation of the main function that calls all of the other functions
template <class T> PyObject *rasterReader<T>::runGetImage() {
    ENTER("run Get Image");
    if (setInitialVariables() == 0) {
        WARN("image not in viewport: returning to python");
        return Py_BuildValue("OOO", Py_None, Py_None, Py_None);
    }

    calculateValues();
    checkImgPixSize();
    dataslice[0][0] = dspRastTop;
    dataslice[0][1] = dspRastTop + dspRastYSize;
    dataslice[1][0] = dspRastLeft;
    dataslice[1][1] = dspRastLeft + dspRastXSize;
    if (dataslice[0][1] - dataslice[0][0] > dspHeightC) {
        dataslice[0][0] = 0;
        dataslice[0][1] = dspHeightC;
    }
    if (dataslice[1][1] - dataslice[1][0] > dspWidthC) {
        dataslice[1][0] = 0;
        dataslice[1][1] = dspWidthC;
    }

    fillMask();
    // need to check the python type of the bands object
    // this is because it sometimes switches between a list and
    // a tuple, which have different access implementations..
    if (strcmp(bands->ob_type->tp_name, "list") == 0) {
        OUT("bands is of type list");
        if ((int)PyList_Size((PyObject *)bands) == 3) {
            return rgbLut(false);
        } else {
            return singleLut(false);
        }
    } else {
        OUT("bands is of type tuple");
        if ((int)PyTuple_Size((PyObject *)bands) == 3) {
            return rgbLut(true);
        } else {
            return singleLut(true);
        }
    }
}

// set the inital values of most of the global class variables.
// This is so we only need to call python functions right at the beginning
// of the run.
template <class T> int rasterReader<T>::setInitialVariables() {
    ENTER("SetInitialVariables");
    this->coordmgr = PyObject_GetAttrString(this->self, "coordmgr");
    this->stretch = PyObject_GetAttrString(this->self, "stretch");
    this->pixTopC =
        PyFloat_AsDouble(PyObject_GetAttrString(this->coordmgr, "pixTop"));
    this->pixBottomC =
        PyFloat_AsDouble(PyObject_GetAttrString(this->coordmgr, "pixBottom"));
    this->pixRightC =
        PyFloat_AsDouble(PyObject_GetAttrString(this->coordmgr, "pixRight"));
    this->pixLeftC =
        PyFloat_AsDouble(PyObject_GetAttrString(this->coordmgr, "pixLeft"));
    this->dspHeightC =
        PyLong_AsLong(PyObject_GetAttrString(this->coordmgr, "dspHeight"));
    this->dspWidthC =
        PyLong_AsLong(PyObject_GetAttrString(this->coordmgr, "dspWidth"));
    this->RasterYSizeC = (long)this->gdalDataset[0]->GetRasterYSize();
    this->RasterXSizeC = (long)this->gdalDataset[0]->GetRasterXSize();
    if (checkCoords() == 0)
        return 0;

    this->noDataValues =
        (PyListObject *)PyObject_GetAttrString(this->self, "noDataValues");
    this->bands = PyObject_GetAttrString(this->stretch, "bands");

    this->imgPixPerWinPixCoord = PyFloat_AsDouble(
        PyObject_GetAttrString(this->coordmgr, "imgPixPerWinPix"));
    this->MASK_IMAGE_VALUE = (long)PyLong_AsLong(
        PyObject_GetAttrString(this->viewerLUT, "MASK_IMAGE_VALUE"));
    this->MASK_BACKGROUND_VALUE = (long)PyLong_AsLong(
        PyObject_GetAttrString(this->viewerLUT, "MASK_BACKGROUND_VALUE"));
    this->MASK_NODATA_VALUE = (long)PyLong_AsLong(
        PyObject_GetAttrString(this->viewerLUT, "MASK_NODATA_VALUE"));
    // loop through and open the current GDALDatset and put it into the array
    // this is so we have seperate handles for the dataset for use with threads
    // should check to see if bandsize is > 1 before this. so we aren't
    // needlessy opening multiple handles.
    for (int i = 1; i < 3; i++) {
        this->gdalDataset[i] = (GDALDataset *)GDALOpen(
            this->gdalDataset[0]->GetFileList()[0], GA_ReadOnly);
    }

    this->data = std::vector<T>(this->dspHeightC * this->dspWidthC, 0);
    this->datalist = new std::vector<std::vector<T>>;
    this->mask = new std::vector<uint8_t>;
    EXIT("Set Initial Variables");
    return 1;
}

// check the coords of the image, and whether they are in the viewport
// if not return as there's nothing to display and no point moving on.
template <class T> int rasterReader<T>::checkCoords() {
    ENTER("checkCoords");
    if ((this->pixTopC < 0 && this->pixBottomC < 0) ||
        (this->pixLeftC < 0 && this->pixRightC < 0)) {
        WARN("image not in viewport < 0");
        return 0;
    } else if ((this->pixTopC > this->RasterYSizeC &&
                this->pixBottom > this->RasterYSizeC) ||
               (this->pixLeftC > this->RasterXSizeC &&
                this->pixRightC > this->RasterXSizeC)) {
        WARN("image not in viewport > RasterSize");
        return 0;
    } else
        EXIT("checkCoords");
    return 1;
}

// Calculate the values of remaining variables. we do this after checking
// whether the
// image is in the viewport so that we aren't pointlessly doing calculations.
template <class T> int rasterReader<T>::calculateValues() {
    ENTER("calculateValues");
    this->fullrespixperovpix = PyFloat_AsDouble(
        PyObject_GetAttrString(this->selectedovi, "fullrespixperpix"));
    this->indexC =
        PyLong_AsLong(PyObject_GetAttrString(this->selectedovi, "index"));
    this->pixTopC = (double)std::max(this->pixTopC, (double)0.0);
    this->pixBottomC =
        (double)std::min(this->pixBottomC, (double)this->RasterYSizeC);
    this->pixLeftC = (double)std::max(this->pixLeftC, (double)0.0);
    this->pixRightC =
        (double)std::min(this->pixRightC, (double)this->RasterXSizeC);
    this->ovtop =
        std::max((long)(this->pixTopC / this->fullrespixperovpix), (long)0);
    this->ovbottom = (long)std::min(
        (long)(ceil(this->pixBottomC / this->fullrespixperovpix)),
        (long)PyLong_AsLong(
            PyObject_GetAttrString(this->selectedovi, "ysize")));
    VAR("fullrespixperovpix: " + std::to_string(this->fullrespixperovpix));
    VAR("ovbottom" + std::to_string(this->ovbottom));
    this->ovleft =
        std::max((long)(this->pixLeftC / this->fullrespixperovpix), (long)0);
    this->ovright =
        (long)std::min((long)(ceil(this->pixRightC / this->fullrespixperovpix)),
                       (long)PyLong_AsLong(
                           PyObject_GetAttrString(this->selectedovi, "xsize")));
    VAR("ovright" + std::to_string(this->ovright));
    this->ovxsize = this->ovright - this->ovleft;
    VAR("ovxsize" + std::to_string(this->ovxsize));
    this->ovysize = this->ovbottom - this->ovtop;
    VAR("ovysize" + std::to_string(this->ovysize));
    PyObject *result = PyObject_CallMethod(this->coordmgr, "pixel2displayF",
                                           "dd", this->pixLeftC, this->pixTopC);
    PyArg_ParseTuple(result, "dd", &this->dspRastLeftC, &this->dspRastTopC);
    result = PyObject_CallMethod(this->coordmgr, "pixel2displayF", "dd",
                                 this->pixRightC, this->pixBottomC);
    PyArg_ParseTuple(result, "dd", &this->dspRastRightC, &this->dspRastBottomC);
    this->dspRastXSize =
        (long)(round(this->dspRastRightC - this->dspRastLeftC));
    this->dspRastYSize =
        (long)(round(this->dspRastBottomC - this->dspRastTopC));
    this->dspRastTop = (long)this->dspRastTopC;
    this->dspRastBottom = this->dspRastTop + this->dspRastYSize;
    this->dspRastLeft = (long)this->dspRastLeftC;
    this->dspRastRight = this->dspRastLeft + this->dspRastXSize;
    this->pixTop = (long)floor(this->pixTopC);
    this->pixBottom = (long)ceil(this->pixBottomC);
    this->pixLeft = (long)floor(this->pixLeftC);
    this->pixRight = (long)ceil(this->pixRightC);
    EXIT("calculateValues");
    return 1;
}

// check the img pixel size per window pixel size.
// if imgpixsize/winpixsize < 1 then we need to get some
// extra size info.
template <class T> void rasterReader<T>::checkImgPixSize() {
    ENTER("checkImgPixSize");
    if (this->imgPixPerWinPixCoord < 1) {
        VAR("imgPixPerWinPixCoord >= 1: val: " +
            std::to_string(this->imgPixPerWinPixCoord));
        PyArg_ParseTuple(PyObject_CallMethod(this->coordmgr, "pixel2display",
                                             "ii", this->pixLeft, this->pixTop),
                         "ll", &this->dspRastAbsLeft, &this->dspRastAbsTop);
        PyArg_ParseTuple(PyObject_CallMethod(this->coordmgr, "pixel2display",
                                             "ii", this->pixRight,
                                             this->pixBottom),
                         "ll", &this->dspRastAbsRight, &this->dspRastAbsBottom);
        this->dspTopExtra = ((this->dspRastTop - this->dspRastAbsTop) /
                             this->fullrespixperovpix);
        this->dspBottomExtra = ((this->dspRastAbsBottom - this->dspRastBottom) /
                                this->fullrespixperovpix);
        this->dspLeftExtra = ((this->dspRastLeft - this->dspRastAbsLeft) /
                              this->fullrespixperovpix);
        this->dspRightExtra = ((this->dspRastAbsBottom - this->dspRastBottom) /
                               this->fullrespixperovpix);
        this->dspRightExtra = std::max(this->dspRightExtra, (double)0.0);
        this->dspBottomExtra = std::max(this->dspBottomExtra, (double)0.0);
    }
    EXIT("checkImgPixSize");
}

// fill the mask which will be used for all bands of the raster
// only need to d this once per raster.
template <class T> void rasterReader<T>::fillMask() {
    ENTER("fillMask");
    OUT("resizing mask to: " + std::to_string(this->dspHeightC) + ", " +
        std::to_string(this->dspWidthC) + " : " +
        std::to_string(this->dspHeightC * this->dspWidthC));
    this->mask->resize(this->dspHeightC * this->dspWidthC,
                       this->MASK_BACKGROUND_VALUE);
    OUT("dataslice is: " +
        std::to_string(this->dataslice[0][1] - this->dataslice[0][0]) + ", " +
        std::to_string(this->dataslice[1][1] - this->dataslice[1][0]));
    for (long x = this->dataslice[0][0]; x < this->dataslice[0][1]; ++x) {
        for (long y = this->dataslice[1][0]; y < this->dataslice[1][1]; ++y) {
            this->mask->at(arrPos(x, y, this->dspWidthC)) =
                this->MASK_IMAGE_VALUE;
        }
    }
    EXIT("fillMask");
}

// main function for a 3 band raster. this creates the threads and calls the
// other functions.
template <class T> PyObject *rasterReader<T>::rgbLut(bool tuple) {
    ENTER("rgbLut");
    int bandSize;
    long bandNum;
    if (tuple) {
        bandSize = (int)PyTuple_Size(this->bands);
    } else {
        bandSize = (int)PyList_Size(this->bands);
    }
    VAR("Bandsize is : " + std::to_string(bandSize));
    this->datalist->resize(bandSize);
    // create and start 3 threads(1 per band. May need to check max amount of
    // threads
    // availiable to cpu before this, but 3 threads should be safe for most
    // modern cpus.
    std::future<int> handle[3];
    for (int i = 0; i < bandSize; i++) {
        handle[i] = std::async(std::launch::async, &rasterReader<T>::runLoop,
                               this, (i + 1), bandSize, tuple);
    }
    int i = 0;
    // wait for each thread to finish as we don't want to carry on until we
    // have all of the data.
    for (i = 0; i < bandSize; i++) {
        while (handle[i].wait_for(std::chrono::seconds(1)) !=
               std::future_status::ready) {
        }
    }
    noDataMaskFill();
    EXIT("rgbLUT after return");
    return build_PyReturn(true, bandSize);
}

// main function for a single band image, this one doesn't create any extra
// threads.
template <class T> PyObject *rasterReader<T>::singleLut(bool tuple) {
    ENTER("singleLut");
    int bandSize = 1;
    this->datalist->resize(bandSize);
    if (runLoop(1, bandSize, tuple) == -1) {
        OUT("runLoop returning -1, raster not in viewPort - cleaning up and "
            "returning to python");
        clearVariables();
        EXIT("singleLut");
        return Py_BuildValue("OOO", Py_None, Py_None, Py_None);
    }
    this->data = this->datalist->at(0);
    noDataMaskFill();
    EXIT("singleLut after return");
    return build_PyReturn(false, bandSize);
}

// main function that does all of the main function calls.
// only calls functions, no calculations are done in this function
template <class T>
int rasterReader<T>::runLoop(int currentBand, int bandSize, bool tuple) {
    long bandNum;
    if (tuple) {
        bandNum = PyLong_AsLong(
            (PyObject *)PyTuple_GetItem(this->bands, currentBand - 1));
    } else {
        bandNum = PyLong_AsLong(
            (PyObject *)PyList_GetItem(this->bands, currentBand - 1));
    }
    GDALRasterBand *band = gdalDataset[currentBand - 1]->GetRasterBand(bandNum);
    this->data.resize(this->dspHeightC * this->dspWidthC);
    std::vector<T> localData =
        std::vector<T>(this->dspHeightC * this->dspWidthC, 0);
    if (this->indexC > 0) {
        band = band->GetOverview(this->indexC - 1);
    }
    // check whether we are zoomed in or not as we have
    // different implementations depending.
    if (this->imgPixPerWinPixCoord >= 1.0) {
        localData = fillData_zoomed_out(band);
    } else {
        localData = fillData_zoomed_in(band);
        if (localData.size() == 1)
            return -1;
    }
    noDataSetup(bandNum, localData);
    // lock the mutex whilst we access datalist as it's a global variable and we
    // don't want to be accessing it at the same time, big problems will occur.
    this->rasterIO_mutex.lock();
    this->datalist->at(currentBand - 1) = localData;
    this->rasterIO_mutex.unlock();

    return 1;
}

// setup the nodata value and array. This needs to be mutex locked
// as it needs to access global variables
// although may be able to change that and speed it up a bit.
template <class T>
void rasterReader<T>::noDataSetup(int bandNum, std::vector<T> localData) {
    std::lock_guard<std::mutex> guard(rasterIO_mutex);
    ENTER("noDataSetup");
    long nodata_value;
    std::vector<int> inimage_and_nodata(this->dspHeightC * this->dspWidthC,
                                        0);
    if (PyList_GetItem((PyObject *)this->noDataValues, (bandNum - 1))
                                                                  != Py_None) {
        nodata_value = (long)PyLong_AsLong(
                PyList_GetItem((PyObject *)this->noDataValues, (bandNum - 1)));
        VAR("nodata_value: " + std::to_string(nodata_value));
    } else {
        nodata_value = NULL;
        VAR("nodata_value: NULL");
    }
    inimage_and_nodata = logical_and_arrays(
        compareArr8bitToInt(this->mask, this->MASK_IMAGE_VALUE),
        compareArrToInt(localData, nodata_value));
    if (this->nodata_mask.empty()) {
        OUT("nodata_mask is empty, setting to inimage_and_nodata");
        this->nodata_mask = inimage_and_nodata;
    } else {
        OUT("nodata_mask not empty, anding with inimage_and_nodata");
        this->nodata_mask =
            logical_and_arrays(this->nodata_mask, inimage_and_nodata);
    }
    EXIT("noDataSetup");
}

// builds the return arrays which will be sent back to python
// these are using the numpy C api to convert
// the C arrays to numpy arrays.
template <class T>
PyObject *rasterReader<T>::build_PyReturn(bool rgb, int bandSize) {
    ENTER("build_PyReturn");
    T *Ttype;
    PyObject *returnVal;
    npy_intp dims[2] = {this->dspHeightC, this->dspWidthC};
    OUT("Creating returnMask");
    PyArrayObject *returnMask =
        ((PyArrayObject *)PyArray_SimpleNew(2, dims, NPY_UINT8));
    uint8_t *maskArr = (uint8_t *)PyArray_DATA(returnMask);
    memcpy(maskArr, this->mask->data(), sizeof(uint8_t) * this->mask->size());
    if (rgb) {
        OUT("rgb image, creating return list");
        PyListObject *pyDatalist = (PyListObject *)(PyList_New(3));
        for (int i = 0; i < bandSize; i++) {
            PyList_SetItem((PyObject *)pyDatalist, i,
                           PyArray_SimpleNew(
                               2, dims, gdalToNPYDatatype(gdalDataTypeName)));
            T *dataArr = (T *)PyArray_DATA(
                (PyArrayObject *)PyList_GetItem((PyObject *)pyDatalist, i));
            memcpy(dataArr, this->datalist->at(i).data(),
                   sizeof(T) * this->datalist->at(i).size());
        }
        returnVal = Py_BuildValue("OOO", pyDatalist, Py_None, returnMask);
        Py_DECREF(pyDatalist);
    } else {
        PyArrayObject *returnData = ((PyArrayObject *)PyArray_SimpleNew(
            2, dims, gdalToNPYDatatype(gdalDataTypeName)));
        T *dataArr = (T *)PyArray_DATA(returnData);
        memcpy(dataArr, data.data(), sizeof(T) * data.size());
        returnVal = Py_BuildValue("OOO", Py_None, returnData, returnMask);
        Py_DECREF(returnData);
    }
    Py_DECREF(returnMask);
    clearVariables();
    EXIT("build_PyReturn - clearing up and returning to python");
    return returnVal;
}

// fill the no data mask
template <class T> void rasterReader<T>::noDataMaskFill() {
    ENTER("noDataMaskFill");
    if (!this->nodata_mask.empty()) {
        OUT("No data mask not empty");
        for (long x = 0; x < this->dspHeightC; ++x) {
            for (long y = 0; y < this->dspWidthC; ++y) {
                this->mask->at(arrPos(x, y, this->dspWidthC)) =
                    this->nodata_mask[arrPos(x, y, this->dspWidthC)] ==
                            this->MASK_NODATA_VALUE
                        ? this->nodata_mask[arrPos(x, y, this->dspWidthC)]
                        : this->mask->at(arrPos(x, y, this->dspWidthC));
            }
        }
    }
    EXIT("noDataMaskFill");
}

// function which does all of the calculations and calls GDAL RasterIO for
// zoomed out rasters(imgPixPerWinPix >1.0
template <class T>
std::vector<T> rasterReader<T>::fillData_zoomed_out(GDALRasterBand *band) {
    T *dataTmp =
        (T *)CPLMalloc(sizeof(T) * (this->dspRastXSize * this->dspRastYSize));
    std::vector<T> localData =
        std::vector<T>(this->dspWidthC * this->dspHeightC);
    band->RasterIO(GF_Read, this->ovleft, this->ovtop, this->ovxsize,
                   this->ovysize, (void *)dataTmp, this->dspRastXSize,
                   this->dspRastYSize, band->GetRasterDataType(), 0, 0);
    long count = 0;
    for (long x = this->dataslice[0][0]; x < this->dataslice[0][1]; ++x) {
        for (long y = this->dataslice[1][0]; y < this->dataslice[1][1]; ++y) {
            localData[arrPos(x, y, this->dspWidthC)] = dataTmp[count];
            count++;
        }
    }
    CPLFree(dataTmp);
    return localData;
}

// function which calls RasterIO for zoomed in images. This also calls
// replicateArray which does the pixel interpolation so we can fill the display
// port
template <class T>
std::vector<T> rasterReader<T>::fillData_zoomed_in(GDALRasterBand *band) {
    T *dataTmp = (T *)CPLMalloc(sizeof(T) * (this->ovxsize * this->ovysize));
    band->RasterIO(GF_Read, this->ovleft, this->ovtop, this->ovxsize,
                   this->ovysize, dataTmp, this->ovxsize, this->ovysize,
                   band->GetRasterDataType(), 0, 0);
    std::vector<T> dataTmp2 = replicateArray((T *)dataTmp);
    if (dataTmp2.size() == 1) {
        return dataTmp2;
    }
    std::vector<T> localData =
        std::vector<T>(this->dspWidthC * this->dspHeightC);
    long count = 0;
    for (long x = this->dataslice[0][0]; x < this->dataslice[0][1]; ++x) {
        for (long y = this->dataslice[1][0]; y < this->dataslice[1][1]; ++y) {
            localData[arrPos(x, y, this->dspWidthC)] = dataTmp2[count];
            count++;
        }
    }
    CPLFree(dataTmp);
    return localData;
}

//Replicates the data array so that if fits the display height/width
//this is called when the user has zoomed in on the image to a point
//where a single image pixel is less than a single window pixel.
template <class T> std::vector<T> rasterReader<T>::replicateArray(T *dataTmp) {
    ENTER("replicateArray");
    long height = this->dataslice[0][1] - this->dataslice[0][0];
    long width = this->dataslice[1][1] - this->dataslice[1][0];
    long nrows = this->ovysize;
    long ncols = this->ovxsize;
    double nRptsX =
        (double)((width + this->dspLeftExtra + this->dspRightExtra) /
                 (double)ncols);
    double nRptsY =
        (double)((height + this->dspTopExtra + this->dspBottomExtra) /
                 (double)nrows);
    long colCount = (long)ceil(ncols * nRptsX);
    long rowCount = (long)ceil(nrows * nRptsY);
    long nYNum = (colCount - this->dspRightExtra) - this->dspLeftExtra;
    long nXNum = (rowCount - this->dspBottomExtra) - this->dspTopExtra;
    if (nXNum <= 0 || nYNum <= 0)
        return std::vector<T>(1, 0);
    std::vector<double> cols;
    std::vector<double> rows;
    meshgrid(this->dspTopExtra, rowCount - this->dspBottomExtra,
             this->dspLeftExtra, colCount - this->dspRightExtra, &cols, &rows);
    multiply(&cols, ncols / (double)colCount, this->dspTopExtra,
             rowCount - this->dspBottomExtra, this->dspLeftExtra,
             colCount - this->dspRightExtra);
    multiply(&rows, nrows / (double)rowCount, this->dspTopExtra,
             rowCount - this->dspBottomExtra, this->dspLeftExtra,
             colCount - this->dspRightExtra);
    std::vector<T> tmpReturnArr(nXNum * nYNum, 0);
    long posXtmp, posYtmp;
    for (long x = 0; x < nXNum; ++x) {
        for (long y = 0; y < nYNum; ++y) {
            posXtmp = (long)rows[arrPos(x, y, nYNum)];
            posYtmp = (long)cols[arrPos(x, y, nYNum)];
            tmpReturnArr[arrPos(x, y, nYNum)] =
                (T)dataTmp[arrPos(posXtmp, posYtmp, this->ovxsize)];
        }
    }
    EXIT("replicateArray after return");
    return resizeArray(tmpReturnArr, nXNum, nYNum, height, width);
}

//Creates two meshgrids(horizontal and vertical) in a manner
//similar to numpy.mgrid. Needed for basic interpolation
//of the image pixels.
template <class T>
void rasterReader<T>::meshgrid(double startx, double endx, double starty,
                               double endy, std::vector<double> *cols,
                               std::vector<double> *rows) {
    ENTER("meshgrid");
    cols->resize((endx - startx) * (endy - starty));
    rows->resize((endx - startx) * (endy - starty));
    for (long x = 0; x < endx - startx; ++x) {
        for (long y = 0; y < endy - starty; ++y) {
            cols->at(arrPos(x, y, (endy - starty))) = y + starty;
            rows->at(arrPos(x, y, (endy - starty))) = x + startx;
        }
    }
    EXIT("meshgrid");
}

//multiplies each value in an array by condition. and forces
//the output to be positive in cases that the condition was negative.
template <class T>
void rasterReader<T>::multiply(std::vector<double> *arr, double multiply,
                               long startx, long endx, long starty, long endy) {
    ENTER("multiply");
    for (long x = 0; x < endx - startx; ++x) {
        for (long y = 0; y < endy - starty; ++y) {
            arr->at(arrPos(x, y, (endy - starty))) *= multiply;
            arr->at(arrPos(x, y, (endy - starty))) *=
                arr->at(arrPos(x, y, (endy - starty))) < 0 ? -1 : 1;
        }
    }
    EXIT("multiply");
}

//resizes the array to imgWidth by imgHeight, so that i fits
//into the display.
template <class T>
std::vector<T> rasterReader<T>::resizeArray(std::vector<T> arr, long arrWidth,
                                            long arrHeight, long imgWidth,
                                            long imgHeight) {
    ENTER("resizeArray");
    std::vector<T> returnArr(imgWidth * imgHeight, 0);
    long count = 0;
    for (long x = 0; x < imgWidth; ++x) {
        for (long y = 0; y < imgHeight; ++y) {
            if (arr.size() > x * y) {
                returnArr[count] = arr[arrPos(x, y, arrHeight)];
            }
            count++;
        }
    }
    EXIT("resizeArray");
    return returnArr;
}


//compares an array of type T to an integer condition
//returning a boolean array.
template <class T>
std::vector<int> rasterReader<T>::compareArrToInt(std::vector<T> arr,
                                                  long condit) {
    ENTER("compareArrToInt" + condit);
    std::vector<int> boolArr(arr.size(), 0);
    T tmp = 0;
    for (long x = 0; x < this->dspHeightC; ++x) {
        for (long y = 0; y < this->dspWidthC; ++y) {
            tmp = arr.at(arrPos(x, y, this->dspWidthC));
            boolArr[arrPos(x, y, this->dspWidthC)] = tmp == condit ? 1 : 0;
        }
    }
    EXIT("compareArrToInt" + condit);
    return boolArr;
}


//compares an 8 bit integer array to an integer condition
//returning a boolean array
template <class T>
std::vector<int> rasterReader<T>::compareArr8bitToInt(std::vector<uint8_t> *arr,
                                                      int condit) {
    ENTER("compareMask" + std::to_string(condit));
    std::vector<int> boolArr(arr->size(), 0);
    int tmp = 0;
    for (long x = 0; x < this->dspHeightC; ++x) {
        for (long y = 0; y < this->dspWidthC; ++y) {
            tmp = (int)arr->at(arrPos(x, y, this->dspWidthC));
            boolArr[arrPos(x, y, this->dspWidthC)] = tmp == condit ? 1 : 0;
        }
    }
    EXIT("compareMask" + std::to_string(condit));
    return boolArr;
}

//logically ANDS two arrays together returning a boolean array
//ie. ||arr1 || arr2 || arrout||
//    ||0    || 0    || 0     ||
//    ||0    || >1   || 0     ||
//    ||>1   || 0    || 0     ||
//    ||>1   || >1   || 1     ||
template <class T>
std::vector<int> rasterReader<T>::logical_and_arrays(std::vector<int> arr1,
                                                     std::vector<int> arr2) {
    ENTER("logical_and_arrays");
    std::vector<int> boolArr(arr1.size(), 0);
    for (long x = 0; x < this->dspHeightC; ++x) {
        for (long y = 0; y < this->dspWidthC; ++y) {
            boolArr[arrPos(x, y, this->dspWidthC)] =
                arr1[arrPos(x, y, this->dspWidthC)] &&
                arr2[arrPos(x, y, this->dspWidthC)];
        }
    }
    EXIT("logical_and_arrays");
    return boolArr;
}


//given a gdal data type name, returns the equivelent numpy data type enum.
template <class T>
enum NPY_TYPES rasterReader<T>::gdalToNPYDatatype(const char *gdalDataType) {
    if (strcmp(gdalDataType, "Byte") == 0)
        return NPY_UINT8;
    else if (strcmp(gdalDataType, "UInt16") == 0)
        return NPY_UINT16;
    else if (strcmp(gdalDataType, "Int16") == 0)
        return NPY_INT16;
    else if (strcmp(gdalDataType, "UInt32") == 0)
        return NPY_UINT32;
    else if (strcmp(gdalDataType, "Int32") == 0)
        return NPY_INT32;
    else if (strcmp(gdalDataType, "Float32") == 0)
        return NPY_FLOAT32;
    else if (strcmp(gdalDataType, "Float64") == 0)
        return NPY_FLOAT64;
    else
        return NPY_VOID;
}


//clean up function rather than using a destructor
//sets variables to null, closes gdal datasets and
//clears vectors.
template <class T> void rasterReader<T>::clearVariables() {
    ENTER("clearVariables");
    this->fullrespixperovpix = this->selectedOviFullResPix =
        this->imgPixPerWinPixCoord = NULL;
    this->pixTopC = this->pixBottomC = this->pixLeftC = this->pixRightC = NULL;
    this->dspRastRightC = this->dspRastLeftC = this->dspRastTopC =
        this->dspRastBottomC = NULL;
    this->dspLeftExtra = this->dspRightExtra = this->dspTopExtra =
        this->dspBottomExtra = NULL;
    this->ovtop = this->ovleft = this->ovbottom = this->ovright =
        this->ovxsize = this->ovysize = NULL;
    this->dspRastRight = this->dspRastLeft = this->dspRastTop =
        this->dspRastBottom = NULL;
    this->dspRastXSize = this->dspRastYSize = NULL;
    this->dspRastAbsLeft = this->dspRastAbsTop = this->dspRastAbsRight =
        this->dspRastAbsBottom = NULL;
    this->RasterYSizeC = this->RasterXSizeC = this->indexC = this->dspHeightC =
        this->dspWidthC = NULL;
    this->pixTop = this->pixBottom = this->pixLeft = this->pixRight = NULL;
    Py_DECREF(noDataValues);
    this->selectedovi = this->coordmgr = this->stretch = NULL;
    this->mask->clear();
    this->nodata_mask.clear();
    this->data.clear();
    this->datalist->clear();
    for (int i = 0; i < 3; i++) {
        GDALClose(gdalDataset[i]);
    }
    EXIT("clearVariables");
}

