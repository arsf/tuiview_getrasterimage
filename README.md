# TuiView C++ getRasterImage Plugin #

## About ##

A C++ plugin for [TuiView](http://tuiview.org/) to provide improved performance
compared to the default Python/NumPy implementation, in particular when a large
number of layers are loaded.

Written by Gareth Jones.

## Licensing ##

This software is available under the General Public License (GPL) Version 3.
See the file 'LICENSE' for more details.

## Installation ##

To install use.

```
python setup.py install
```
Currently the NERC-ARF-DAN fork of TuiView is required to use this plugin, this is available from:
https://bitbucket.org/arsf/tuiview

To check the plugin is install use the 'Help' -> 'About' dialogue. If correctly
installed will list:

```
C++ getRasterImage: Yes
```

## Known Issues ##

Currently only supports Python 2.7. Python 3 support is planned.
Please report other issues / bugs by opening an issue.
